import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user.interface';
import {
  AuthResponse,
  CustomError,
  ForgotPasswordResponse,
  VerifiedTokenResponse,
} from '../interfaces/auth.interface';
import { Observable, catchError, throwError, map, tap, of } from 'rxjs';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  private _user?: User;

  get user() {
    return { ...this._user };
  }

  login(
    mail: string,
    password: string
  ): Observable<AuthResponse | CustomError> {
    const url = `${this.baseUrl}/auth/login`;
    return this.http.post<AuthResponse>(url, { mail, password }).pipe(
      tap((resp: AuthResponse) => {
        console.log(resp, '<== resp');
        if (resp.ok) {
          localStorage.setItem('token', resp.token!);
          this._user = resp.user;
        }
      }),
      catchError((err) => {
        let errorInfo: CustomError;
        if (err.error.message === 'Usuario inactivo') {
          errorInfo = {
            error: true,
            message: err.error.message,
            status: err.status,
          };
        } else if (err.error.message === 'Correo incorrecto') {
          errorInfo = {
            error: true,
            message: err.error.message,
            status: err.status,
          };
        } else if (err.error.message === 'Contraseña incorrecta') {
          errorInfo = {
            error: true,
            message: err.error.message,
            status: err.status,
          };
        } else {
          errorInfo = {
            error: true,
            message: 'Error desconocido',
            status: err.status,
          };
        }
        return of(errorInfo);
      })
    );
  }

  register(
    name: string,
    password: string,
    rut: string,
    mail: string
  ): Observable<AuthResponse> {
    return this.http
      .post<AuthResponse>(`${this.baseUrl}/auth/register`, {
        name,
        password,
        rut,
        mail,
      })
      .pipe(
        catchError((error: HttpErrorResponse) => {
          const errorMessage = error.error.message || 'Ocurrió un error';
          return throwError(new Error(errorMessage));
        })
      );
  }

  verifyAccount(token: string): Observable<VerifiedTokenResponse> {
    return this.http.post<VerifiedTokenResponse>(
      `${this.baseUrl}/auth/verifyAccount/${token}`,
      {}
    );
  }

  forgotPassword(mail: string) {
    return this.http
      .post<ForgotPasswordResponse>(`${this.baseUrl}/auth/forgotPassword`, {
        mail,
      })
      .pipe(
        catchError((error: HttpErrorResponse) => {
          const errorMessage = error.error.message || 'Ocurrió un error';
          return throwError(new Error(errorMessage));
        })
      );
  }

  resetPassword(newPassword: string, verificationToken: string) {
    return this.http.post<ForgotPasswordResponse>(
      `${this.baseUrl}/auth/resetPassword`,
      { newPassword, verificationToken }
    );
  }

  validateToken(): Observable<boolean> {
    const headers = new HttpHeaders().set(
      'token',
      localStorage.getItem('token') || ''
    );
    const token = localStorage.getItem('token') || '';

    return this.http
      .post(`${this.baseUrl}/auth/validateToken`, { token }, { headers })
      .pipe(
        map((resp: any) => {
          this._user = resp.user;
          return true;
        }),
        catchError((err) => of(false))
      );
  }
}
