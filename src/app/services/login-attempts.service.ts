import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IPInfo, LoginAttempt } from '../interfaces/attempts.interface';

@Injectable({
  providedIn: 'root',
})
export class LoginAttemptsService {
  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  newLoginAttempt(
    mail: string,
    ipAddress: string,
    location: string,
    country: string
  ): Observable<LoginAttempt> {
    return this.http.post<LoginAttempt>(`${this.baseUrl}/attempt`, {
      mail,
      ipAddress,
      location,
      country,
    });
  }

  getUserIp(): Observable<IPInfo> {
    return this.http.get<IPInfo>('https://ipapi.co/json/');
  }
}
