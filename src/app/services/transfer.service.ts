import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  NewTransferResponse,
  UserTransferResponse,
} from '../interfaces/transfer.interface';
import { getToken } from './utils';

@Injectable({
  providedIn: 'root',
})
export class TransferService {
  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  getTransferByUser(clientId: string): Observable<UserTransferResponse> {
    return this.http.get<UserTransferResponse>(
      `${this.baseUrl}/transfer/userTransfer/${clientId}`,
      { headers: getToken() }
    );
  }

  newTransfer(
    issuer: string,
    rutReceiver: string,
    amount: number,
    accountNumber: number,
    userMail: string
  ): Observable<NewTransferResponse> {
    return this.http
      .post<NewTransferResponse>(
        `${this.baseUrl}/transfer/new`,
        { issuer, rutReceiver, amount, accountNumber, userMail },
        {
          headers: getToken(),
        }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => {
          const errorMessage = error.error.message || 'Ocurrió un error';
          return throwError(new Error(errorMessage));
        })
      );
  }
}
