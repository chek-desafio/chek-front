import { HttpHeaders } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

export const getToken = () => {
  return new HttpHeaders().set('token', localStorage.getItem('token') || '');
};

export const formatRut = (event: any, form: FormGroup): void => {
  if (event.target.value === null || event.target.value === undefined) {
    return;
  }
  let input = event.target.value.replace(/[^0-9kK]/g, '');

  if (input.length > 9) {
    input = input.slice(0, 9);
  }

  let rut = '';

  if (input.length <= 1) {
    rut = input;
  } else {
    let num = input.slice(0, -1);
    const dv = input.slice(-1);

    if (num.length > 6) {
      rut = `${num.slice(0, -6)}.${num.slice(-6, -3)}.${num.slice(-3)}-${dv}`;
    } else if (num.length > 3) {
      rut = `${num.slice(0, -3)}.${num.slice(-3)}-${dv}`;
    } else {
      rut = `${num}-${dv}`;
    }
  }

  form.get('rut')?.setValue(rut, { emitEvent: false });
};

export const validarRut = (rut: string): boolean => {
  if (rut === null || rut === undefined) {
    return false;
  }

  rut = rut.replace(/[.-]/g, '');
  if (rut.length < 8 || rut.length > 9) {
    return false;
  }
  var dv = rut.slice(-1);
  var numero = rut.slice(0, -1);
  var suma = 0;
  var mul = 2;
  for (var i = numero.length - 1; i >= 0; i--) {
    suma += parseInt(numero[i], 10) * mul;
    if (mul === 7) {
      mul = 2;
    } else {
      mul++;
    }
  }
  var rest: number | string = 11 - (suma % 11);
  if (rest === 11) {
    rest = 0;
  } else if (rest === 10) {
    rest = 'k';
  }
  if (dv.toLowerCase() !== rest.toString()) {
    return false;
  }
  return true;
};
