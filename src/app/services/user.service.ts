import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserResponse } from '../interfaces/user.interface';
import { getToken } from './utils';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
  private baseUrl: string = environment.baseUrl;

  getInfoUser(clientId: string): Observable<UserResponse> {
    return this.http.get<UserResponse>(
      `${this.baseUrl}/user/${clientId}`,
      { headers: getToken() }
    );
  }
}
