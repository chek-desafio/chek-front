import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { VerifyComponent } from './components/verify/verify.component';
import { VerifiedComponent } from './components/verified/verified.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ForgotPasswordMailComponent } from './components/forgot-password-mail/forgot-password-mail.component';
import { ResetPasswordVerifiedComponent } from './components/reset-password-verified/reset-password-verified.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'verify',
    component: VerifyComponent,
  },
  {
    path: 'verified/:token',
    component: VerifiedComponent,
  },
  {
    path: 'forgotPassword',
    children: [
      {
        path: '',
        component: ForgotPasswordComponent,
      },
      {
        path: 'mail',
        component: ForgotPasswordMailComponent,
      },
    ],
  },
  {
    path: 'resetPassword',
    children: [
      {
        path: 'token/:token',
        component: ResetPasswordComponent,
      },
      { path: 'verified', component: ResetPasswordVerifiedComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
