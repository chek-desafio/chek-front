import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { validarRut, formatRut } from 'src/app/services/utils';
import { DialogLoginComponent } from '../shared/dialog-login/dialog-login.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private titleService: Title,
    private dialog: MatDialog
  ) {}

  rutValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const valid = validarRut(control.value);
      return valid ? null : { invalidRut: { value: control.value } };
    };
  }

  registerForm: FormGroup = this.fb.group({
    mail: [
      '',
      [
        Validators.required,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ),
      ],
    ],
    rut: [
      '',
      [
        Validators.required,
        Validators.pattern(/^(\d{1,2}\.\d{3}\.\d{3}-[\dkK])$/),
        this.rutValidator(),
      ],
    ],
    fullName: [
      '',
      [
        Validators.required,
        Validators.pattern(/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/),
      ],
    ],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });
  clickButton: boolean = false;
  isLoading: boolean = false;
  ngOnInit(): void {
    this.titleService.setTitle('Chek - Regístrate');
  }

  register(): void {
    if (!this.registerForm.valid) return;
    this.clickButton = true;
    this.isLoading = true;
    this.authService
      .register(
        this.registerForm.get('fullName')?.value,
        this.registerForm.get('password')?.value,
        this.registerForm.get('rut')?.value,
        this.registerForm.get('mail')?.value.toLowerCase()
      )
      .subscribe(
        () => {
          this.router.navigateByUrl('/verify');
        },
        (error) => {
          if (
            error.message ===
            `La cuenta con el correo: ${
              this.registerForm.get('mail')?.value
            } ya se encuentra registrada.`
          ) {
            this.openDialog(error.message);
          } else if (
            error.message ===
            `La cuenta con el rut: ${
              this.registerForm.get('rut')?.value
            } ya se encuentra registrada.`
          ) {
            this.openDialog(error.message);
          }
        }
      );
  }

  openDialog(error: string): void {
    this.registerForm.reset();
    this.isLoading = false;
    this.dialog.open(DialogLoginComponent, {
      data: {
        message: error,
      },
    });
  }

  notValidField(field: string) {
    return (
      this.registerForm.get(field)?.invalid &&
      this.registerForm.get(field)?.touched
    );
  }
  onRutInput(event: any) {
    formatRut(event, this.registerForm);
  }
}
