import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-verified',
  templateUrl: './verified.component.html',
  styleUrls: ['./verified.component.css'],
})
export class VerifiedComponent implements OnInit {
  constructor(
    private router: ActivatedRoute,
    private authService: AuthService,
    private titleService: Title
  ) {}
  isLoading: boolean = true;
  isVerified: boolean = false;

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Verificando...');
    this.router.params.subscribe((param) =>
      this.authService.verifyAccount(param['token']).subscribe((resp) => {
        if (!resp.ok) {
          this.isVerified = false;
          this.isLoading = false;
          this.titleService.setTitle('Chek - Ya validaste tu cuenta!');
        } else {
          this.isVerified = true;
          this.isLoading = false;
          this.titleService.setTitle('Chek - Bienvenido a chek 💜!');
        }
      })
    );
  }
}
