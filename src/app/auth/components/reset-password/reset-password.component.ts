import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private titleService: Title,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  resetPasswordForm: FormGroup = this.fb.group({
    password: ['', [Validators.required, Validators.minLength(6)]],
  });
  verifiedToken: string = '';
  isLoading: boolean = false;
  notValidField(field: string) {
    return (
      this.resetPasswordForm.get(field)?.invalid &&
      this.resetPasswordForm.get(field)?.touched
    );
  }
  resetPassword() {
    if (!this.resetPasswordForm.valid) return;
    this.isLoading = true;
    this.authService
      .resetPassword(
        this.resetPasswordForm.get('password')!.value,
        this.verifiedToken
      )
      .subscribe((resp) => {
        if (resp.ok) this.router.navigateByUrl('/resetPassword/verified');
      });
  }

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Resetear contraseña');
    this.route.params.subscribe((param) => {
      console.log(param['token']);
      this.verifiedToken = param['token'];
    });
  }
}
