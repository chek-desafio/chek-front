import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-reset-password-verified',
  templateUrl: './reset-password-verified.component.html',
  styleUrls: ['./reset-password-verified.component.css']
})
export class ResetPasswordVerifiedComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Cambio exitóso de contraseña')
  }

}
