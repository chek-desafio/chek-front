import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogForgotPasswordComponent } from '../shared/dialog-forgot-password/dialog-forgot-password.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private titleService: Title,
    private router: Router,
    private dialog: MatDialog
  ) {}

  isLoading: boolean = false;

  forgotPasswordForm: FormGroup = this.fb.group({
    mail: [
      '',
      [
        Validators.required,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ),
      ],
    ],
  });

  notValidField(field: string) {
    return (
      this.forgotPasswordForm.get(field)?.invalid &&
      this.forgotPasswordForm.get(field)?.touched
    );
  }

  forgotPassword() {
    if (!this.forgotPasswordForm.valid) return;
    this.isLoading = true;
    this.authService
      .forgotPassword(this.forgotPasswordForm.get('mail')!.value)
      .subscribe(
        () => {
          this.router.navigateByUrl('/forgotPassword/mail');
        },
        (error) => {
          this.openDialog();
          this.forgotPasswordForm.reset();
          this.isLoading = false;
        }
      );
  }

  openDialog(): void {
    this.dialog.open(DialogForgotPasswordComponent);
  }

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Recuperar contraseña');
  }
}
