import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';

import { VerifyComponent } from './components/verify/verify.component';
import { VerifiedComponent } from './components/verified/verified.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ForgotPasswordMailComponent } from './components/forgot-password-mail/forgot-password-mail.component';
import { ResetPasswordVerifiedComponent } from './components/reset-password-verified/reset-password-verified.component';
import { DialogForgotPasswordComponent } from './components/shared/dialog-forgot-password/dialog-forgot-password.component';
import { DialogLoginComponent } from './components/shared/dialog-login/dialog-login.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    VerifyComponent,
    VerifiedComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ForgotPasswordMailComponent,
    ResetPasswordVerifiedComponent,
    DialogForgotPasswordComponent,
    DialogLoginComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatDialogModule,
  ],
})
export class AuthModule {}
