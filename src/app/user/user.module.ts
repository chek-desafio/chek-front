import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserRoutingModule } from './user-routing.module';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PanelComponent } from './components/panel/panel.component';
import { SharedModule } from '../shared/shared.module';
import { TransferComponent } from './components/transfer/transfer.component';
import { LastMovementsComponent } from './components/dashboard/last-movements/last-movements.component';
import { BalanceComponent } from './components/shared/balance/balance.component';
import { DoneComponent } from './components/transfer/done/done.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PanelComponent,
    TransferComponent,
    LastMovementsComponent,
    BalanceComponent,
    DoneComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    MatSidenavModule,
    MatTableModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
  ],
})
export class UserModule {}
