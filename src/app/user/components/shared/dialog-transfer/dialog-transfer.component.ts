import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-transfer',
  templateUrl: './dialog-transfer.component.html',
  styleUrls: ['./dialog-transfer.component.css'],
})
export class DialogTransferComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DialogTransferComponent>
  ) {}

  ngOnInit(): void {}

  onOkButtonClick(): void {
    this.dialogRef.close('OK');
  }
}
