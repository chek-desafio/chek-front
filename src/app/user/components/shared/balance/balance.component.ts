import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css'],
})
export class BalanceComponent implements OnInit {
  @Input() clientId!: string;
  balance!: number;
  isLoading: boolean = true;
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.userService.getInfoUser(this.clientId).subscribe((resp) => {
      this.balance = resp.user.balance;
      this.isLoading = false;
    });
  }
}
