import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TransferService } from 'src/app/services/transfer.service';
import { formatRut, validarRut } from 'src/app/services/utils';
import { MatDialog } from '@angular/material/dialog';
import { DialogTransferComponent } from '../shared/dialog-transfer/dialog-transfer.component';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css'],
})
export class TransferComponent implements OnInit {
  get user() {
    return this.authService.user;
  }

  constructor(
    private fb: FormBuilder,
    private titleService: Title,
    private transferService: TransferService,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  rutValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const valid = validarRut(control.value);
      return valid ? null : { invalidRut: { value: control.value } };
    };
  }

  transferForm: FormGroup = this.fb.group({
    mail: [
      '',
      [
        Validators.required,
        Validators.pattern(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ),
      ],
    ],
    rut: [
      '',
      [
        Validators.required,
        Validators.pattern(/^(\d{1,2}\.\d{3}\.\d{3}-[\dkK])$/),
        this.rutValidator(),
      ],
    ],
    fullName: [
      '',
      [
        Validators.required,
        Validators.pattern(/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/),
      ],
    ],
    accountNumber: ['', [Validators.required]],
    amount: ['', [Validators.required, Validators.maxLength(7)]],
  });

  isLoading: boolean = false;

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Transferencias');
  }

  notValidField(field: string) {
    return (
      this.transferForm.get(field)?.invalid &&
      this.transferForm.get(field)?.touched
    );
  }

  onRutInput(event: any) {
    formatRut(event, this.transferForm);
  }

  numbersOnlyInput(event: any) {
    let input = event.target.value.replace(/[^0-9]/, '');

    if (input.length > 7) {
      input = input.slice(0, 7);
    }

    this.transferForm.get('amount')?.setValue(input, { emitEvent: false });
  }

  newTransfer() {
    if (!this.transferForm.valid) return;
    this.isLoading = true;
    this.transferService
      .newTransfer(
        this.user._id!,
        this.transferForm.get('rut')!.value,
        parseInt(this.transferForm.get('amount')!.value),
        parseInt(this.transferForm.get('accountNumber')!.value),
        this.transferForm.get('mail')!.value
      )
      .subscribe(
        () => {
          this.router.navigateByUrl('/user/transfer/done');
          this.isLoading = false;
        },
        (error) => {
          if (error.message === 'Saldo inferior al monto a transferir') {
            this.openDialog(error.message);
          } else if (error.message === 'El usuario no existe') {
            this.openDialog(error.message);
          }
        }
      );
  }

  openDialog(error: string): void {
    this.transferForm.reset();
    this.isLoading = false;
    this.dialog.open(DialogTransferComponent, {
      data: {
        message: error,
      },
    });
  }
}
