import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserTransferResponse } from 'src/app/interfaces/transfer.interface';
import { AuthService } from 'src/app/services/auth.service';
import { TransferService } from 'src/app/services/transfer.service';
import { LastMovementsComponent } from './last-movements/last-movements.component';
import { BalanceComponent } from '../shared/balance/balance.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  get user() {
    return this.authService.user;
  }

  @ViewChild(LastMovementsComponent)
  lastMovementsComponent!: LastMovementsComponent;
  @ViewChild(BalanceComponent) balanceComponent!: BalanceComponent;
  constructor(private authService: AuthService, private titleService: Title) {}

  ngOnInit(): void {
    this.titleService.setTitle('Chek - Dashboard');
  }

  reloadComponents() {
    this.lastMovementsComponent.ngOnInit();
    this.balanceComponent.ngOnInit();
  }
}
