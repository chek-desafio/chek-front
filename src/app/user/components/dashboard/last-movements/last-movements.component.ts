import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  Transfer,
} from 'src/app/interfaces/transfer.interface';
import { TransferService } from 'src/app/services/transfer.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-last-movements',
  templateUrl: './last-movements.component.html',
  styleUrls: ['./last-movements.component.css'],
})
export class LastMovementsComponent implements OnInit {
  @Input() clientId!: string;

  dataLastMovements!: Transfer[];
  displayedColumns: string[] = ['createdAt', 'issuer', 'receiver', 'amount'];
  dataSource!: MatTableDataSource<Transfer>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  isLoading: boolean = true;

  constructor(
    public userService: UserService,
    private transferService: TransferService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.transferService.getTransferByUser(this.clientId!).subscribe((resp) => {
      this.dataLastMovements = resp.transfers;
      this.dataSource = new MatTableDataSource(this.dataLastMovements);

      this.dataSource.sortingDataAccessor = (item: any, property: string) => {
        switch (property) {
          case 'issuer':
            return item.issuer.name;
          case 'receiver':
            return item.receiver.name;
          default:
            return item[property];
        }
      };

      setTimeout(() => (this.dataSource.paginator = this.paginator));
      setTimeout(() => (this.dataSource.sort = this.sort));

      this.isLoading = false;
    });
  }
}
