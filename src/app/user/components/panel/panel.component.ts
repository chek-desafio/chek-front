import { Component, OnInit, HostListener } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css'],
})
export class PanelComponent implements OnInit {
  opened: boolean = false;
  noFullAdmin: boolean = false;
  menuMode: MatDrawerMode = 'over'; 

  constructor(private router: Router) {}

  ngOnInit(): void {}

  signOut(): void {
    localStorage.removeItem('token'); 
    this.router.navigateByUrl('');
  }
}
