import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-transfer-type',
  templateUrl: './transfer-type.component.html',
  styleUrls: ['./transfer-type.component.css'],
})
export class TransferTypeComponent implements OnInit {
  @Input() type!: string;
  @Input() amount!: number;
  constructor() {}

  ngOnInit(): void {}
}
