import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output() changeOpened = new EventEmitter<boolean>();

  get user() {
    return this.authService.user;
  }
  constructor(private authService: AuthService) {}

  changeOpenedValue() {
    this.changeOpened.emit();
  }
  ngOnInit(): void {
    // console.log('USUARIO => ', this.authService.usuario);
  }
}
