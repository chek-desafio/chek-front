import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-animated',
  templateUrl: './skeleton-animated.component.html',
  styleUrls: ['./skeleton-animated.component.css']
})
export class SkeletonAnimatedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
