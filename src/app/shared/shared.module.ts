import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ClpFormatPipe } from '../pipes/clp-format.pipe';
import { TransferTypeComponent } from './components/transfer-type/transfer-type.component';
import { SkeletonAnimatedComponent } from './components/skeleton-animated/skeleton-animated.component';

@NgModule({
  declarations: [
    HeaderComponent,
    ClpFormatPipe,
    TransferTypeComponent,
    SkeletonAnimatedComponent,
  ],
  imports: [CommonModule],
  exports: [
    HeaderComponent,
    ClpFormatPipe,
    TransferTypeComponent,
    SkeletonAnimatedComponent,
  ],
})
export class SharedModule {}
