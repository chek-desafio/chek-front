import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'clpFormat',
})
export class ClpFormatPipe implements PipeTransform {
  transform(value: number): string {
    if (value == 0) {
      return `$${value}`;
    }
    if (value > 0) {
      const formattedValue = value
        .toFixed(0)
        .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return `$${formattedValue}`;
    }

    return '';
  }
}
